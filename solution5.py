import random

grid_size = 5


def initialize_game_grid():
    grid = []
    for i in range(grid_size):
        row = []
        for j in range(grid_size):
            row.append("O")
        grid.append(row)

    return grid


def get_failure_message(turn):
    if turn < 2:
        return "You missed my battleship!"
    else:
        return "Game Over"


def print_grid(grid):
    for i in range(grid_size):
        for j in range(grid_size):
            print(grid[i][j], end="")
        print()


def generate_random_location_for_ship():
    return random.randint(1, grid_size), random.randint(1, grid_size)


def start_game():
    print("Let's play Battleship!")

    # Place the ship
    ships_row_location, ships_column_location = generate_random_location_for_ship()

    # Initialize game's grid
    game_grid = initialize_game_grid()

    guess_count = 0
    while guess_count < 3:
        print_grid(game_grid)
        guess_row = int(input("Guess Row:"))
        guess_column = int(input("Guess Col:"))
        if guess_row == ships_row_location and guess_column == ships_column_location:
            print("You sinked the ship! Well done!")
            game_grid[guess_row - 1][guess_column - 1] = "S"
            print_grid(game_grid)
            return
        else:
            game_grid[guess_row - 1][guess_column - 1] = "X"
            print(get_failure_message(guess_count))
            guess_count += 1
            print("Turn:", guess_count)
    print("The actual location was:")
    print("\tRow=", ships_row_location)
    print("\tColumn=", ships_column_location)


start_game()
