def get_sum_of_the_members_of(input):
    sum = 0
    for number in input:
        sum += number
    return sum


def get_divisors_of(input):
    divisors = []
    for i in range(1, input):
        if input % i == 0:
            divisors.append(i)
    return divisors


def print_out_all_the_perfect_numbers_until(input_range):
    if input_range <= 0:
        print("The input must be a positive integer.")
    abundant_numbers = 0
    deficient_numbers = 0
    for number in range(1, input_range + 1):
        divisors = get_divisors_of(number)
        sum = get_sum_of_the_members_of(divisors)
        if sum == number:
            print(number, "is a perfect number.")
        elif sum > number:
            abundant_numbers += 1
        else:
            deficient_numbers += 1
    print("There are", abundant_numbers, "abundant numbers between", 0, "and", input_range, ".")
    print("There are", deficient_numbers, "deficient numbers between", 0, "and", input_range, ".")


# Perfect numbers: 6, 28, 496, 8128, 33550336 ...
print_out_all_the_perfect_numbers_until(100)
# print_out_all_the_perfect_numbers_until(1000)
# print_out_all_the_perfect_numbers_until(10000)
# print_out_all_the_perfect_numbers_until(35000000)
