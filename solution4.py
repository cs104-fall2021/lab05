def transpose(matrix):
    transposed_matrix = []
    for i in range(len(matrix[0])):
        new_row = []
        for j in range(len(matrix)):
            new_row.append(matrix[j][i])
        transposed_matrix.append(new_row)
    return transposed_matrix


# input: [[1, 2], [3, 4], [5, 6]]
my_matrix = [[1, 2], [3, 4], [5, 6]]
# expected: [[1, 3, 5], [2, 4, 6]]
print(transpose(my_matrix))
