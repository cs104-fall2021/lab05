def find_greatest_common_divisor(first_number, second_number):
    while first_number % second_number != 0:
        temporary_number = first_number
        first_number = second_number
        second_number = temporary_number % second_number
    return second_number


print(find_greatest_common_divisor(6, 12))
