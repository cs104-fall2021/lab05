def is_palindrome(word):
    clean_word = ""
    for character in word:
        if character.isalpha():
            clean_word = clean_word + character.lower()
    for i in range(len(clean_word) // 2):
        if clean_word[i].lower() != clean_word[-(i + 1)].lower():
            return False
    return True


# expect True: deed, radar, level, madam
for word in ["d eEd", "raDar", " leVel?", "no lemon! no melon!", "...borrow or Rob!"]:
    print("is the word \"" + word + "\" palindrome?:", is_palindrome(word))

# expect False: lime, fish, system, future
for word in ["liMe", "!fish", "system?", "future", "Ozyegin University"]:
    print("is the word \"" + word + "\" palindrome?:", is_palindrome(word))
